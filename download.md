---
layout: page
title: Download
permalink: /download/
sitemap: true
---

Kaidan's source code can be found at [invent.kde.org](https://invent.kde.org/kde/kaidan).

### Windows

Nightly builds for Windows are available as a [standalone binary](https://invent.kde.org/kde/kaidan/-/jobs/artifacts/master/raw/kaidan.exe?job=windows-mxe).

### Linux

Kaidan is available as AppImage and Flatpak:
 * [Flatpak (stable)](https://flathub.org/apps/details/im.kaidan.kaidan) ([nightly](https://invent.kde.org/kde/kaidan/wikis/install/flatpak))
 * [AppImage (nightly)](https://invent.kde.org/kde/kaidan/-/jobs/artifacts/master/raw/Kaidan-continuous-x86_64.AppImage?job=linux-appimage)

### macOS

We provide [nightly builds for macOS](https://invent.kde.org/kde/kaidan/-/jobs/artifacts/master/raw/kaidan.tar?job=mac-osxcross).

### Ubuntu Touch

<a target="new" href="https://open-store.io/app/im.kaidan.kaidan"><img width="200px" src="/images/download-openstore.jpg"/></a>

Alternatively, you can get the latest nightly builds from [here](https://invent.kde.org/kde/kaidan/-/jobs/artifacts/master/browse?job=click-xenial).

### Android

Very experimental nightly builds (based on the KDE Frameworks master branch) are available.
Please note that they are not signed with a proper key, and updating the app is only possible by re-installing it.

[Android APK](https://invent.kde.org/kde/kaidan/-/jobs/artifacts/master/raw/kaidan_build_apk-debug.apk?job=android)
