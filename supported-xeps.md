---
layout: page
title: Supported XMPP Features
permalink: /features/
sitemap: true
---

### XMPP Extension Protocols (XEPs)

These are all the XEPs supported by Kaidan.

| XEP                                                                                 | Version | Comment|
| ------------------------------------------------------------------------------------| ------- | ------ |
| [XEP-0030](https://xmpp.org/extensions/xep-0030.html): Service Discovery            | v2.2    |        |
| [XEP-0054](https://xmpp.org/extensions/xep-0054.html): vcard-temp                   | v1.2    | Only used for avatars |
| [XEP-0091](https://xmpp.org/extensions/xep-0091.html): Legacy Delayed Delivery      | v1.4    | Read-only |
| [XEP-0153](https://xmpp.org/extensions/xep-0153.html): vCard-Based Avatars          | v1.0    |        |
| [XEP-0184](https://xmpp.org/extensions/xep-0184.html): Message Delivery Receipts    | v1.2    |        |
| [XEP-0199](https://xmpp.org/extensions/xep-0199.html): Ping                         | v2.0    |        |
| [XEP-0203](https://xmpp.org/extensions/xep-0203.html): Delayed Delivery             | v0.1    |        |
| [XEP-0280](https://xmpp.org/extensions/xep-0280.html): Message Carbons              | v0.8    |        |
| [XEP-0308](https://xmpp.org/extensions/xep-0308.html): Last Message Correction      | v1.0    |        |
| [XEP-0352](https://xmpp.org/extensions/xep-0352.html): Client State Indication      | v0.2    |        |
| [XEP-0363](https://xmpp.org/extensions/xep-0363.html): HTTP File Upload             | v0.9    |        |
| [XEP-0382](https://xmpp.org/extensions/xep-0382.html): Spoiler messages             | v0.2    |        |

### XMPP Core RFCs

Kaidan supports these RFCs:
* RFC-6120: Extensible Messaging and Presence Protocol (XMPP): Core
* RFC-6121: Extensible Messaging and Presence Protocol (XMPP): Instant Messaging and Presence

### Planned XEPs

For file sharing we want to use Stateless Inline Media Sharing (SIMS) ([XEP-0385](https://xmpp.org/extensions/xep-0385.html)). This would allow us to also transmit a thumbnail (using Bits of Binary), a media type, file size and checksums. SIMS is based on HTTP File Upload and can be implemented in a backwards-compatible way. SIMS also encourages developers to implement both HTTP File Upload and Jingle File Transfer, but we'll only use HTTP File Upload because it is widely deployed already and Jingle File Transfer has many downsides.

We want to use MIX for groupchats instead of MUC. There is already an experimental server implementations in ejabberd and as soon as there's also one in prosody and MUC <-> MIX mirroring works we won't have any disadvantages with it.

For e2ee we want to use OMEMO with the ATT extension (Automatic Trust Transfer).
