---
layout: page
title: Development
permalink: /development/
sitemap: true
---

Our source code is managed using Git. You can browse it on [our GitLab instance](https://invent.kde.org/KDE/kaidan) or clone it locally using:

{% highlight bash %}
git clone https://invent.kde.org/KDE/kaidan
{% endhighlight %}

### Contributing

Issues and code review is done on KDE's GitLab. You need to register a [KDE Identity account](https://identity.kde.org/) to start contributing.

### Contact

You can get in touch with us in our [XMPP channel](https://i.kaidan.im/#kaidan@muc.kaidan.im?join).

