---
layout: post
title: "Kaidan 0.4.0 released!"
date: 2019-07-08 15:00:00 +02:00
author: zatrox
---

![]({{ "/images/2019/07/08/screenshot-0.4.png" | prepend: site.url }})

It's finally here!

After more than one and a half years there finally is a new release. Kaidan
0.4.0 is the biggest update until now and apart from some bug-fixes and many
minor and major features increasing the usability, Kaidan now has
multiplatform-support for all common operating systems like Linux, Windows,
Android and macOS.

But have a look at the changelog yourself:

## Changelog

Build system:

* Support for Android (ilyabizyaev)
* Support for Ubuntu Touch (jbb)
* Support for macOS (ilyabizyaev)
* Support for Windows (ilyabizyaev)
* Support for iOS (ilyabizyaev)
* Add KDE Flatpak (jbb)
* Switch Android builds to CMake with ECM (ilyabizyaev)
* Improve Linux AppImage build script (ilyabizyaev)
* Add additional image formats in AppImage (jbb)

New features:

* Show proper notifications using KNotifications (lnj)
* Add settings page for changing passwords (jbb, lnj)
* Add XEP-0352: Client State Indication (gloox/QXmpp) (lnj)
* Add media/file (including GIFs) sharing (lnj, jbb)
* Full back-end rewrite to QXmpp (lnj)
* Implement XEP-0363: HTTP File Upload and UploadManager for QXmpp (lnj)
* Use XEP-0280: Message Carbons from QXmpp (lnj)
* Use XEP-0352: Client State Indication from QXmpp (lnj)
* Check incoming messages for media links (lnj)
* Implement XEP-0308: Last Message Correction (lnj, jbb)
* Make attachments downloadable (lnj)
* Implement XEP-0382: Spoiler messages (xavi)
* Kaidan is now offline usable (lnj)
* Kaidan is able to open xmpp: URIs (lnj)
* New logo (ilyabizyaev)
* Show presence information of contacts (lnj, melvo)
* Add EmojiPicker from Spectral with search and favorites functionality (jbb, fazevedo)
* Highlight links in chat and make links clickable (lnj)
* New about dialog instead of the about page (ilyabizyaev)
* Add image preview in chat and before sending (lnj)
* Send messages on Enter, new line on Ctrl-Enter (ilyabizyaev)
* 'Add contact' is now the main action on the contacts page (lnj)
* Elide contact names and messages in roster (lnj)
* Chat page redesign (ilyabizyaev)
* Display passive notifications when trying to use online actions while offline (lnj)
* Automatically reconnect on connection loss (lnj)
* Contacts page: Display whether online in title (lnj)
* Add different connection error messages (jbb)
* Use QApplication when building with QWidgets (notmart)
* Ask user to approve subscription requests (lnj)
* Remove contact action: Make JIDs bold (lnj)
* Add contact sheet: Ask for optional message to contact (lnj)
* Add empty chat page with help notice to be displayed on start up (jbb)
* Redesign log in page (sohnybohny)
* Add Copy Invitaion URL action (jbb)
* Add 'press and hold' functionality for messages context menu (jbb)
* Add copy to clipboard function for messages (jbb)
* Add mobile file chooser (jbb)
* Highlight the currently opened chat on contacts page (lnj)
* Remove predefined window sizes (lnj)
* Use new Kirigami application header (nicofee)
* Make images open externally when clicked (jbb)
* Use QtQuickCompiler (jbb)
* Display upload progress bar (lnj)
* Add text+color avatars as fallback (lnj, jbb)
* Remove diaspora log in option (lnj)

Misc:

* Forget passwords on log out (lnj)
* Append four random chars to resource (lnj)
* Save passwords in base64 instead of clear text (lnj)
* Generate the LICENSE file automatically with all git authors (lnj)
* Store ubuntu touch builds as job artifacts (lnj)
* Add GitLab CI integration (jbb)

Fixes:

* Fix blocking of GUI thread while database interaction (lnj)
* Fix TLS connection bug (lnj)
* Don't send notifications when receiving own messages via. carbons (lnj)
* Fix timezone bug of message timestamps (lnj)
* Fix several message editing bugs (lnj)
* Fix black icons (jbb)
* Fix rich text labels in Plasma Mobile (lnj)
* Small Plasma Mobile fixes (jbb)

Bug reports go to our [issue tracker](https://invent.kde.org/KDE/kaidan/issues) as always and translations are managed on [Weblate](https://hosted.weblate.org/projects/kaidan/translations/).

**Download**:
* [Source code (.tar.xz)](https://download.kde.org/stable/kaidan/0.4.0/kaidan-0.4.0.tar.xz)
* [Windows (x64)](https://download.kde.org/stable/kaidan/0.4.0/kaidan-0.4.0.x86_64.exe)
* [macOS (x64)](https://download.kde.org/stable/kaidan/0.4.0/kaidan-0.4.0.macos.x86_64.tar)
* [Android (armv7) (experimental!)](https://invent.kde.org/kde/kaidan/-/jobs/11232/artifacts/raw/kaidan_build_apk-debug.apk)
* [Ubuntu Touch (armv7) (experimental!)](https://download.kde.org/stable/kaidan/0.4.0/kaidan-0.4.0.armhf.click)
* Flatpak builds are only available as nightlies: **[Installing the Kaidan Flatpak](https://invent.kde.org/KDE/kaidan/wikis/install/flatpak)**

[comment]: # (* [Linux (AppImage)](https://download.kde.org/stable/kaidan/0.4.0/kaidan-0.4.0.x86_64.appimage))

PS: We're searching for someone with an iPhone who can build & test Kaidan for iOS: [contact us](https://i.kaidan.im)!
